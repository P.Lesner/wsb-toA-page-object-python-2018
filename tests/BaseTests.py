import random
import sys

sys.path.insert(0, '..')

from selenium import webdriver
import unittest
from pages.main import MainPage
from pages.login import LoginPage
from pages.board import BoardPage
from tests.TestData import User
from tests.TestData import CardTestData


class BaseTestOnBoardPage(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.maximize_window()
        self.driver.get(LoginPage.address)
        login_page = LoginPage(self.driver)
        login_page.login(User.correct_email, User.correct_password)
        assert login_page.is_login_successful()
        board_name = "TEST BOARD {0}".format(str(random.randint(100, 10000)))
        main_page = MainPage(self.driver)
        main_page.go_to_list_of_boards()
        main_page.create_new_board(board_name, MainPage.BoardType.DEFAULT)
        board_page = BoardPage(self.driver)
        board_page.delete_default_lists_if_such_exist()

        # assert main_page.check_board_created_correctly(board_name)

    def tearDown(self):
        board_page = BoardPage(self.driver)
        board_page.delete_current_board()
        assert board_page.is_board_deleted_correctly()
        self.driver.close()


class BaseTestOnList(BaseTestOnBoardPage):

    def setUp(self):
        super().setUp()
        board_page = BoardPage(self.driver)
        board_page.add_new_list(CardTestData.list_name_1)
        board_page.add_new_list(CardTestData.list_name_2)

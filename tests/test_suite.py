import sys

sys.path.insert(0, '..')
from datetime import datetime
from unittest import TestLoader, TestSuite
from HtmlTestRunner import HTMLTestRunner
from tests.login_logout.LoginTests import LoginTest
from tests.login_logout.LogoutTests import LogoutTest
from tests.lists.CreatingListTests import CreatingListOnTableTest
from tests.lists.ArchivingListTests import ArchivingListTest
from tests.boards.CreateBoardTests import CreateBoardTests
from tests.boards.EditBoardTests import EditBoardTests
from tests.lists.EditingListTests import EditingListOnTableTest
from tests.cards.AddAndDeleteCardTests import AddCardTest, ArchiveAndDeleteCardTest
from tests.cards.EditCardTests import EditCardTests

login_tests = TestLoader().loadTestsFromTestCase(LoginTest)
logout_tests = TestLoader().loadTestsFromTestCase(LogoutTest)
creating_list_tests = TestLoader().loadTestsFromTestCase(CreatingListOnTableTest)
archiving_list_tests = TestLoader().loadTestsFromTestCase(ArchivingListTest)
editing_list_tests = TestLoader().loadTestsFromTestCase(EditingListOnTableTest)
create_board_tests = TestLoader().loadTestsFromTestCase(CreateBoardTests)
edit_board_tests = TestLoader().loadTestsFromTestCase(EditBoardTests)
add_card_test = TestLoader().loadTestsFromTestCase(AddCardTest)
archive_and_delete_card_tests = TestLoader().loadTestsFromTestCase(ArchiveAndDeleteCardTest)
edit_cards_tests = TestLoader().loadTestsFromTestCase(EditCardTests)

directories_name = "reports {0}".format(str(datetime.now().strftime('%Y-%m-%d %H-%M-%S')))
runner = HTMLTestRunner(output=directories_name)

suite = TestSuite(
    [login_tests, logout_tests, create_board_tests, edit_board_tests, creating_list_tests, editing_list_tests,
     archiving_list_tests, add_card_test, archive_and_delete_card_tests, edit_cards_tests])

runner.run(suite)

class CardTestData():
    card_name = "My new card"
    list_name_1 = "My new list 1"
    list_name_2 = "My new list 2"


class TestData():
    # Board data
    test_private_board_name = "Private Test Board"
    test_public_board_name = "Public Test Board"
    test_board_rename = "Rename Test Board"
    test_copy_board_name = "Copy Test Board"
    test_board_name = "test-name"

    # List data
    correct_name_of_list = "test-name"
    empty_name_of_list = " "

    # Team data
    team_name = "Dream Team"
    member_email = "nowyczlonek1@wp.pl"


class User():
    correct_email = 'abrytfanna@gmail.com'
    correct_login = 'abrytfanna'
    correct_password = 'klops456+'
    incorrect_email = 'annabrytfanna@gmail.com'
    incorrect_login = 'incorrect login'
    incorrect_password = 'incorrect password'
    blank_password = ''

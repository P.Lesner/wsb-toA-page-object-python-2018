from HtmlTestRunner import HTMLTestRunner
from selenium import webdriver
import unittest
import sys

sys.path.insert(0, '../..')

from pages.login import LoginPage
from pages.main import MainPage
from tests.TestData import User


class LogoutTest(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.maximize_window()
        self.driver.get(LoginPage.address)
        login_page = LoginPage(self.driver)
        login_page.login(User.correct_email, User.correct_password)

    def tearDown(self):
        self.driver.close()

    def test_logout_successful(self):
        main_page = MainPage(self.driver)
        main_page.logout()
        assert main_page.is_logout_successful()


if __name__ == '__main__':
    unittest.main(testRunner=HTMLTestRunner(output=''))

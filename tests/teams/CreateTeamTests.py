from HtmlTestRunner import HTMLTestRunner
import unittest
from pages.login import LoginPage
from pages.main import MainPage
from pages.main import TeamPage
from tests.TestData import User
from tests.TestData import TestData
from selenium import webdriver


class CreateTeamTests(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.maximize_window()
        self.driver.get(LoginPage.address)

        login_page = LoginPage(self.driver)
        login_page.login(User.correct_email, User.correct_password)
        assert login_page.is_login_successful()
        main_page = MainPage(self.driver)

    def tearDown(self):
        main_page = MainPage(self.driver)
        main_page.logout()
        self.driver.close()

    def test_create_new_team(self):
        team_name = TestData.team_name
        main_page = MainPage(self.driver)
        main_page.create_new_team_from_board_or_team_menu(team_name)
        team_page = TeamPage(self.driver)
        team_page.create_team(team_name)

    def test_adding_member(self):
        member_email = TestData.member_email
        main_page = MainPage(self.driver)
        main_page.add_member()
        team_page = TeamPage(self.driver)
        team_page.add_new_member(member_email)

    def test_delete_team(self):
        team_name = TestData.team_name
        main_page = MainPage(self.driver)
        main_page.delete_team(team_name)

if __name__ == '__main__':
    unittest.main(testRunner=HTMLTestRunner(output=''))

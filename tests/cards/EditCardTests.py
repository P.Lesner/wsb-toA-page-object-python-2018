from HtmlTestRunner import HTMLTestRunner
import unittest
import sys

sys.path.insert(0, "../..")

from pages.board import BoardPage
from pages.card import CardPage
from tests.BaseTests import BaseTestOnList
from tests.TestData import CardTestData


class EditCardTests(BaseTestOnList):

    def test_drag_and_drop_new_card(self):
        board_page = BoardPage(self.driver)
        board_page.add_new_card(CardTestData.card_name)
        board_page.drag_and_drop(CardTestData.card_name, CardTestData.list_name_2)
        assert board_page.list_contains_card(CardTestData.list_name_2, CardTestData.card_name)

    def test_rename_card(self):
        new_card_name = "New card with changed name"
        card_page = CardPage(self.driver)
        board_page = BoardPage(self.driver)
        board_page.add_new_card(CardTestData.card_name)
        assert board_page.is_card_added(CardTestData.card_name)
        card_page.rename_card(new_card_name)
        assert board_page.is_card_name_changed(new_card_name)

    def test_move_card(self):
        card_page = CardPage(self.driver)
        board_page = BoardPage(self.driver)
        board_page.add_new_card(CardTestData.card_name)
        card_page.move_card_to_different_list(CardTestData.list_name_2)
        assert board_page.is_moved_card_message_displayed()
        assert board_page.list_contains_card(CardTestData.list_name_2, CardTestData.card_name)

    def test_add_description_to_card(self):
        new_card_description = "My new card description"
        card_page = CardPage(self.driver)
        board_page = BoardPage(self.driver)
        board_page.add_new_card(CardTestData.card_name)
        card_page.add_description(new_card_description)
        assert card_page.is_description_added(new_card_description)
        card_page.close_card_details()


if __name__ == '__main__':
    unittest.main(testRunner=HTMLTestRunner(output=''))
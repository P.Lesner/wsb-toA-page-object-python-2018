from HtmlTestRunner import HTMLTestRunner
import unittest
import sys

sys.path.insert(0, "../..")

from pages.board import BoardPage
from pages.card import CardPage
from tests.BaseTests import BaseTestOnList
from tests.TestData import CardTestData


class AddCardTest(BaseTestOnList):

    def test_add_new_card(self):
        board_page = BoardPage(self.driver)
        board_page.add_new_card(CardTestData.card_name)
        assert board_page.is_card_added(CardTestData.card_name)


class ArchiveAndDeleteCardTest(BaseTestOnList):

    def test_archive_and_delete_a_card(self):
        card_page = CardPage(self.driver)
        board_page = BoardPage(self.driver)
        board_page.add_new_card(CardTestData.card_name)
        card_page.archive_a_card()
        assert card_page.is_card_archived()
        card_page.delete_card()
        assert not board_page.find_card_by_name(CardTestData.card_name)
        assert board_page.is_deletion_message_displayed()


if __name__ == '__main__':
    unittest.main(testRunner=HTMLTestRunner(output=''))
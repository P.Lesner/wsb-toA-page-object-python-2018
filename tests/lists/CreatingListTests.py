from HtmlTestRunner import HTMLTestRunner
import unittest
import sys

sys.path.insert(0, '../..')
from pages.board import BoardPage
from tests.BaseTests import BaseTestOnBoardPage


class CreatingListOnTableTest(BaseTestOnBoardPage):

    def test_create_list_with_correct_name(self):
        board_page = BoardPage(self.driver)
        list_name = "My to do list"
        board_page.add_new_list(list_name)
        assert board_page.is_list_added(list_name)

    def test_create_few_lists_with_correct_name(self):
        board_page = BoardPage(self.driver)
        lists_names = ["My list A", "My second list B", "Important !!!", "4", ":)", "To do ?"]
        board_page.add_new_lists(lists_names)
        assert board_page.are_lists_added(lists_names)

    def test_create_list_with_whitespace_name(self):
        board_page = BoardPage(self.driver)
        whitespace_name = " "
        board_page.add_new_list(whitespace_name)
        assert not board_page.is_list_added(whitespace_name)

    def test_copy_existing_list(self):
        board_page = BoardPage(self.driver)
        lists_names = ["My list", "Next list", "Final list"]
        board_page.add_new_lists(lists_names)
        board_page.copy_existing_list_and_change_name("Next list", "Next list 2A")
        assert board_page.is_list_added("Next list 2A")


if __name__ == '__main__':
    unittest.main(testRunner=HTMLTestRunner(output=''))

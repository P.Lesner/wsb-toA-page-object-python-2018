from HtmlTestRunner import HTMLTestRunner
import unittest
import sys

sys.path.insert(0, '../..')
from pages.board import BoardPage
from tests.BaseTests import BaseTestOnBoardPage


class EditingListOnTableTest(BaseTestOnBoardPage):

    def test_rename_one_of_two_lists(self):
        board_page = BoardPage(self.driver)
        lists_names = ["LIST1", "LIST2B"]
        board_page.add_new_lists(lists_names)
        board_page.rename_list("LIST1", "My renamed list")
        assert board_page.is_list_renamed("LIST1", "My renamed list")

    def test_rename_all_three_lists(self):
        board_page = BoardPage(self.driver)
        lists_names = ["LIST1", "LIST2B", "TODO"]
        board_page.add_new_lists(lists_names)
        board_page.rename_list("LIST1", "My renamed list")
        board_page.rename_list("LIST2B", "My renamed list2")
        board_page.rename_list("TODO", "My renamed list3")
        assert board_page.is_list_renamed("LIST1", "My renamed list")
        assert board_page.is_list_renamed("LIST2B", "My renamed list2")
        assert board_page.is_list_renamed("TODO", "My renamed list3")


if __name__ == '__main__':
    unittest.main(testRunner=HTMLTestRunner(output=''))

from HtmlTestRunner import HTMLTestRunner
import unittest
import sys

sys.path.insert(0, "../..")

from pages.login import LoginPage
from pages.main import MainPage
from pages.main import BoardPage
from tests.TestData import User
from tests.TestData import TestData
from selenium import webdriver


class CreateBoardTests(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.maximize_window()
        self.driver.get(LoginPage.address)

        login_page = LoginPage(self.driver)
        login_page.login(User.correct_email, User.correct_password)
        assert login_page.is_login_successful()
        main_page = MainPage(self.driver)
        main_page.go_to_list_of_boards()

    def tearDown(self):
        board_page = BoardPage(self.driver)
        board_page.delete_current_board()
        assert board_page.is_board_deleted_correctly()
        main_page = MainPage(self.driver)
        main_page.logout()
        self.driver.close()

    def test_create_new_default_type_board(self):
        board_name = TestData.test_private_board_name
        main_page = MainPage(self.driver)
        main_page.create_new_board(board_name, MainPage.BoardType.DEFAULT)
        board_page = BoardPage(self.driver)
        assert board_page.is_board_created_correctly(board_name)

    def test_create_new_public_board(self):
        board_name = TestData.test_public_board_name
        main_page = MainPage(self.driver)
        main_page.create_new_board(board_name, MainPage.BoardType.PUBLIC)
        board_page = BoardPage(self.driver)
        assert board_page.is_board_created_correctly(board_name)

    def test_create_new_board_left_menu(self):
        board_name = TestData.test_public_board_name
        main_page = MainPage(self.driver)
        main_page.create_new_board_left_menu(board_name, MainPage.BoardType.DEFAULT)
        board_page = BoardPage(self.driver)
        assert board_page.is_board_created_correctly(board_name)

    def test_create_new_board_from_board_or_team_menu(self):
        board_name = TestData.test_public_board_name
        main_page = MainPage(self.driver)
        main_page.create_new_board_from_board_or_team_menu(board_name, MainPage.BoardType.DEFAULT)
        board_page = BoardPage(self.driver)
        assert board_page.is_board_created_correctly(board_name)

    # def test_close_create_new_board_dialog(self):
    #     main_page = MainPage(self.driver)
    #     main_page.click_create_new_board_button()
    #     main_page.click_close_create_new_board_dialog()
    #     assert main_page.is_dialog_closed_correctly()


if __name__ == '__main__':
    unittest.main(testRunner=HTMLTestRunner(output=''))

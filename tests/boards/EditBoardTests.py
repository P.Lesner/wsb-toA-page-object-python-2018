from HtmlTestRunner import HTMLTestRunner
import unittest
import sys

sys.path.insert(0, "../..")

from pages.login import LoginPage
from pages.main import MainPage
from pages.main import BoardPage
from tests.TestData import User
from tests.TestData import TestData
from selenium import webdriver


class EditBoardTests(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Chrome()
        self.driver.maximize_window()
        self.driver.get(LoginPage.address)

        login_page = LoginPage(self.driver)
        login_page.login(User.correct_email, User.correct_password)
        assert login_page.is_login_successful()
        main_page = MainPage(self.driver)
        main_page.go_to_list_of_boards()

    def tearDown(self):
        board_page = BoardPage(self.driver)
        board_page.delete_current_board()
        assert board_page.is_board_deleted_correctly()
        main_page = MainPage(self.driver)
        main_page.logout()
        self.driver.close()

    def __rename_board(self, new_name):
        board_name = TestData.test_private_board_name
        main_page = MainPage(self.driver)
        main_page.create_new_board(board_name, MainPage.BoardType.DEFAULT)
        board_page = BoardPage(self.driver)
        board_page.rename_board(new_name)

    def test_rename_board(self):
        self.__rename_board(TestData.test_board_rename)
        board_page = BoardPage(self.driver)
        assert board_page.is_board_created_correctly(TestData.test_board_rename)

    def test_remove_name_from_existing_board_is_not_possible(self):
        self.__rename_board("")
        board_page = BoardPage(self.driver)
        assert board_page.check_if_rename_button_is_visible()

    def test_change_board_permission_to_public(self):
        board_name = TestData.test_private_board_name
        main_page = MainPage(self.driver)
        main_page.create_new_board(board_name, MainPage.BoardType.DEFAULT)
        board_page = BoardPage(self.driver)
        board_page.change_board_permission_level_to_public()
        assert board_page.is_current_board_public()

    def test_change_board_permission_to_private(self):
        board_name = TestData.test_private_board_name
        main_page = MainPage(self.driver)
        main_page.create_new_board(board_name, MainPage.BoardType.PUBLIC)
        board_page = BoardPage(self.driver)
        board_page.change_board_permission_level_to_private()
        assert board_page.is_current_board_private()

    def test_copy_board(self):
        board_name = TestData.test_private_board_name
        main_page = MainPage(self.driver)
        main_page.create_new_board(board_name, MainPage.BoardType.DEFAULT)
        board_page = BoardPage(self.driver)
        board_page.copy_board(TestData.test_copy_board_name)
        assert board_page.is_board_created_correctly(TestData.test_copy_board_name)

    def test_reopen_board(self):
        board_name = TestData.test_private_board_name
        main_page = MainPage(self.driver)
        main_page.create_new_board(board_name, MainPage.BoardType.DEFAULT)
        board_page = BoardPage(self.driver)
        board_page.close_current_board()
        board_page.reopen_current_board()
        board_page.click_board_menu_back_button()
        assert board_page.is_board_created_correctly(board_name)


if __name__ == '__main__':
    unittest.main(testRunner=HTMLTestRunner(output=''))
import sys

sys.path.insert(0, '..')
from pages.base import BasePage


class LogoutPage(BasePage):
    page_title = "Wylogowany z Trello"

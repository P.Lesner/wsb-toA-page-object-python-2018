import sys

sys.path.insert(0, '../tests')
from pages.base import BasePage
from selenium.webdriver.common.by import By

class TeamPage(BasePage):

    team_name_field = (By.ID, "org-display-name")
    member_field = (By.ID, "add-member-popup")
    create_team_button = (By.CSS_SELECTOR, "#classic > div.pop-over.is-shown > div > div:nth-child(2) > div > div > div > form > input.primary.wide.js-save")
    # member_button = (By.ID, "tabbed-pane-nav-item-button js-org-members active")
    # add_member_button = (By.CLASS_NAME, "icon-sm icon-member")
    # search_member_button = (By.CLASS_NAME, "search-member")
    # checkbox_member = (By.ID, "icon-lg icon-check")
    # adding_button = (By.ID, "Dodaj do zespołu")
    member_email_field = (By.CSS_SELECTOR, ".autocomplete-input")
    adding_submmit = (By.CSS_SELECTOR, ".autocomplete-btn")



    def set_team_name(self, team_name):
        team_element = self.driver.find_element(*TeamPage.team_name_field)
        team_element.clear()
        team_element.send_keys(team_name)

    def click_team_submit(self):
        submit_button = self.driver.find_element(*TeamPage.create_team_button)
        submit_button.click()

    def create_team(self, team_name):
        self.set_team_name(team_name)
        self.click_team_submit()

    def set_email(self, member_email):
        member_element = self.driver.find_element(*TeamPage.member_email_field)
        member_element.clear()
        member_element.send_keys(member_email)

    def click_member_submmit(self):
        submit_button = self.driver.find_element(*TeamPage.adding_submmit)
        submit_button.click()

    def add_new_member(self, member_email):
        self.set_email(member_email)
        self.click_member_submmit()

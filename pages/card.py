from pages.base import BasePage
from pages.board import BoardPage
import sys

sys.path.insert(0, '../tests')
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select


class CardPage(BasePage):

    card_details_window = (By.CLASS_NAME, "card-detail-window")
    card_details_name = (By.CLASS_NAME, "mod-card-back-title")

    archive_card_button = (By.CLASS_NAME, "js-archive-card")
    card_archived_banner = (By.CLASS_NAME, "window-archive-banner-text")
    delete_card_button = (By.CLASS_NAME, "js-delete-card")
    confirm_delete_card_button = (By.CLASS_NAME, "js-confirm")
    card_edit_menu_close_button = (By.CLASS_NAME, "js-close-window")

    change_list_link = (By.CLASS_NAME, "js-open-move-from-header")
    change_list_field = (By.CLASS_NAME, "js-select-list")
    change_list_submit_button = (By.CLASS_NAME, "js-submit")

    card_description_field = (By.CLASS_NAME, "js-desc-content")
    card_description = (By.CLASS_NAME, "js-card-desc")
    card_description_text_area = (By.CLASS_NAME, "js-description-draft")
    confirm_add_description = (By.CLASS_NAME, "mod-submit-edit")

    def archive_a_card(self):
        self.driver.find_element(*BoardPage.name_of_card).click()
        self.wait.until(expected_conditions.visibility_of_element_located(CardPage.card_details_window))
        self.driver.find_element(*CardPage.archive_card_button).click()

    def is_card_archived(self):
        return self.wait.until(expected_conditions.visibility_of_element_located(CardPage.card_archived_banner))

    def delete_card(self):
        delete_button = self.driver.find_element(*CardPage.delete_card_button)
        delete_button.click()
        confirm_delete = self.driver.find_element(*CardPage.confirm_delete_card_button)
        confirm_delete.click()

    def rename_card(self, changed_name):
        self.driver.find_element(*BoardPage.name_of_card).click()
        new_name_field = self.driver.find_element(*CardPage.card_details_name)
        new_name_field.clear()
        new_name_field.send_keys(changed_name)
        new_name_field.send_keys(Keys.ENTER)
        self.driver.find_element(*CardPage.card_edit_menu_close_button).click()

    def move_card_to_different_list(self, name_of_list):
        self.driver.find_element(*BoardPage.name_of_card).click()
        self.driver.find_element(*CardPage.change_list_link).click()
        self.driver.find_element(*CardPage.change_list_field).click()
        select = Select(self.driver.find_element(*CardPage.change_list_field))
        select.select_by_visible_text(name_of_list)
        submit = self.driver.find_element(*CardPage.change_list_submit_button)
        submit.click()
        self.driver.find_element(*CardPage.card_edit_menu_close_button).click()

    def add_description(self, card_description):
        self.driver.find_element(*BoardPage.name_of_card).click()
        card_description_field = self.driver.find_element(*CardPage.card_description_field)
        card_description_field.click()
        card_description_text_area = self.driver.find_element(*CardPage.card_description_text_area)
        card_description_text_area.send_keys(card_description)
        self.driver.find_element(*CardPage.confirm_add_description).click()

    def close_card_details(self):
        self.driver.find_element(*CardPage.card_edit_menu_close_button).click()
        return self.wait.until_not(expected_conditions.presence_of_element_located(CardPage.card_details_window))

    def is_description_added(self, card_description):
        added_description = self.driver.find_element(*CardPage.card_description)
        return added_description.text == card_description







import sys

sys.path.insert(0, '..')
from pages.base import BasePage
from pages.main import MainPage
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions


class LoginPage(BasePage):
    address = "https://trello.com/"

    login_button = (By.LINK_TEXT, 'Zaloguj')
    submit_button = (By.ID, "login")
    email_field = (By.ID, "user")
    password_field = (By.ID, "password")

    login_error_message_selector = (By.CSS_SELECTOR, "#error > p")
    login_error_message_incorrect_email = 'To nie jest konto dla tego e-maila'
    login_error_message_incorrect_password = 'Nieprawidłowe hasło'

    page_title = "Trello"

    def click_login(self):
        self.wait.until(expected_conditions.presence_of_element_located(LoginPage.login_button))
        self.wait.until(expected_conditions.element_to_be_clickable(LoginPage.login_button))
        login_page_button = self.driver.find_element(*LoginPage.login_button)
        login_page_button.click()

    def set_email(self, email):
        email_element = self.driver.find_element(*LoginPage.email_field)
        email_element.clear()
        email_element.send_keys(email)

    def set_password(self, password):
        password_element = self.driver.find_element(*LoginPage.password_field)
        password_element.clear()
        password_element.send_keys(password)

    def click_submit(self):
        submit_button = self.driver.find_element(*LoginPage.submit_button)
        submit_button.click()

    # def login_error_displayed(self):
    #     self.wait.until(expected_conditions.visibility_of_element_located(LoginPage.login_error_message_selector))
    #     notification_element = self.driver.find_element(*LoginPage.login_error_message_selector)
    #     return notification_element.is_displayed()

    def incorrect_email_error_displayed(self):
        self.wait.until(expected_conditions.visibility_of_element_located(LoginPage.login_error_message_selector))
        notification_element = self.driver.find_element(*LoginPage.login_error_message_selector)
        return notification_element.text == self.login_error_message_incorrect_email

    def incorrect_password_error_displayed(self):
        self.wait.until(expected_conditions.visibility_of_element_located(LoginPage.login_error_message_selector))
        notification_element = self.driver.find_element(*LoginPage.login_error_message_selector)
        return notification_element.text == self.login_error_message_incorrect_password

    def is_login_successful(self):
        # page_title = self.driver.find_element(*LoginPage.login_page_title)
        # return page_title.is_displayed()
        self.wait.until(expected_conditions.title_is(MainPage.main_page_title or MainPage.page_title))
        current_page_title = self.driver.title
        return current_page_title == MainPage.main_page_title or current_page_title == MainPage.page_title

    def login(self, email, password):
        self.click_login()
        self.set_email(email)
        self.set_password(password)
        self.click_submit()
        # self.wait.until(expected_conditions.title_is(MainPage.main_page_title or MainPage.page_title))
